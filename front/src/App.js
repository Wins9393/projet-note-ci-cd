import './App.css';
import { useState, useEffect } from 'react';

const App = () => {
  const [profsApi, setProfsApi] = useState([]);

  useEffect(() => {
    fetchProfs();
  }, []);

  const fetchProfs = async () => {
    try{
      const response = await fetch("https://stark-lowlands-77478.herokuapp.com/api/profs?page=1", 
      { 
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
          }
        })

      const data = await response.json()

      setProfsApi(data)
      console.log(data)
    }
    catch(err){
      throw err
    }
  }

  if(profsApi.length){
    return (
      <div className="App">
        <header className="App-header">
        {profsApi.map(({...prof}) => {
          return (
            <div className='profItem' key={prof.id}>
              <p>{prof.firstname}</p> <p>{prof.lastname}</p>
              <p>{prof.streetnumber}, {prof.street}</p>
              <p>{prof.city}</p>
            </div>
          )
        })}
        </header>
      </div>
    );
  }
  else{
    return (
      <div className="App">
        <header className="App-header">
          <h1>Loading...</h1>
        </header>
      </div>
    )
  }
}

export default App;
